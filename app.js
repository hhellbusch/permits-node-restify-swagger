'use strict';

var SwaggerRestify = require('swagger-restify-mw');
var SwaggerUi = require('swagger-tools/middleware/swagger-ui');
var restify = require('restify');
const corsMiddleware = require('restify-cors-middleware')


var app = restify.createServer();

if (process.stdout._handle) {
  process.stdout._handle.setBlocking(true);
}

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

SwaggerRestify.create(config, function(err, swaggerRestify) {
  if (err) { throw err; }
  
  // install swagger ui - listens on /docs and /api-docs
  // /docs is the typical swagger ui for showing and interacting with the various
  // end points.
  // /api-docs is a JSON response showing the capabilities and end points of the server
  // NOTE: this must be before the swaggerRestify.register
  app.use(SwaggerUi(swaggerRestify.runner.swagger));

  // install middleware for swagger
  swaggerRestify.register(app);

  //set up CORS
  const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ['API-Token'],
    exposeHeaders: ['API-Token-Expiry']
  });
  app.pre(cors.preflight);
  app.use(cors.actual);

  

  // set up an error handler
  // app.use(function(err, req, res, next){
  //   console.log("custom error hanlder called");
  //   if (typeof err !== 'object') {
  //     err = {
  //       message: String(err)
  //     };
  //   } else {
  //     // Ensure that err.message is enumerable (It is not by default
  //     Object.defineProperty(err, 'message', {enumerable: true});
  //   }
  //   res.statusCode = 500;
  //   res.json(err);
  // });

  // finally - 
  var port = process.env.PORT || 10011;
  app.listen(port);
  
  // generated code for running the hello-world example
  // if (swaggerRestify.runner.swagger.paths['/hello']) {
  //   console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  // }
});
