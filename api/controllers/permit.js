'use strict';

var util = require('util');
var db = require('../../config/db')();

// example eletrical permit application 
// https://www.columbus.gov/WorkArea/DownloadAsset.aspx?id=68349

module.exports = {
  addElectrical: addElectrical
};


 
function addElectrical(req, res) {
  // console.log("%j", req.body);
  // return res;
  let jobSiteLocation = req.body.jobSiteLocation;
  let propertyOwner = req.body.propertyOwner;
  let permitHolder = req.body.permitHolder;

  let permit = {
    jobSiteLocation: jobSiteLocation,
    propertyOwner: propertyOwner,
    permitHolder: permitHolder,
    type: "ELECTRICAL",
    status: "IN_PROGRESS"
  };

  let result = db.save("permit", permit);
  var responseObj = {
    "success" : result,
    "data" : permit,
    "description": "Electrical permit application submitted"
  };
  // console.log("%j", responseObj);
  // res.json(responseObj);
  res.contentType = 'json';
  res.send(responseObj);
}
